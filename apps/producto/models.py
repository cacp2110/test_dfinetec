from django.db import models

class Producto(models.Model):
    nombre = models.CharField(max_length=50)
    stock = models.IntegerField()
    precio_compre = models.IntegerField()
    precio_venta = models.IntegerField()
    cant_vendida = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre