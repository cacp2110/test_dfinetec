from django.conf.urls import url, include

from apps.producto.views import index , ProductoList , ProductoCreate , ProductoUpdate

urlpatterns = [
    url(r'^$',index),
    url(r'^listar$',ProductoList.as_view(),name='producto_listar'),
    url(r'^nuevo$',ProductoCreate.as_view(),name='producto_crear'),
    url(r'^editar/(?P<pk>\d+)$',ProductoUpdate.as_view(),name='producto_editar'),

]