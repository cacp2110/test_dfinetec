from django.shortcuts import render , redirect
from django.http import HttpResponse
from apps.cliente.forms import ClienteForm
from apps.cliente.models import Cliente
from django.urls import reverse_lazy
from django.views.generic import ListView , CreateView , UpdateView , DetailView

def index(request):
    return render(request,'cliente/index.html')

def cliente_view(request):
    if request.method == 'POST':
        form = ClienteForm(request.POST)
        if form.is_valid():
            form.save()
        return render(request,'cliente/index.html')
    else:
        form = ClienteForm()
    return render(request,'cliente/cliente_form.html',{'form':form})

def cliente_list(request):
    cliente = Cliente.objects.all()
    contexto = {'clientes':cliente}
    return render(request,'cliente/cliente_list.html',contexto)

def cliente_edit(request,id_cliente):
    cliente = Cliente.objects.get(id=id_cliente)
    if request.method == 'GET':
        form = ClienteForm (instance = cliente)
    else:
        form = ClienteForm(request.POST,instance= cliente)
        if form.is_valid():
            form.save()
        return redirect('cliente_listar')
    return render(request, 'cliente/cliente_form.html',{'form':form})

class ClienteList(ListView):
    model = Cliente
    template_name = 'cliente/cliente_list.html'

class ClienteCreate(CreateView):
    model=Cliente
    form_class = ClienteForm
    template_name = 'cliente/cliente_form.html'
    success_url = reverse_lazy('cliente_listar')

class ClienteUpdate(UpdateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'cliente/cliente_form.html'
    success_url = reverse_lazy('cliente_listar')

class ClienteDelete(DetailView):
    model = Cliente
    template_name = 'cliente/cliente_delete.html'
    success_url = reverse_lazy('cliente_listar')

