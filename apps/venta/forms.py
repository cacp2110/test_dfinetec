from django import forms

from apps.venta.models import Venta

class VentaForm(forms.ModelForm):

     class Meta:
         model = Venta

         fields = [
             'cliente',
             'producto',

         ]
         labels ={
             'cliente':'Cliente',
             'producto':'Producto',

         }
         widgets={
             'cliente': forms.Select(attrs={'class':'form-control'}),
             'producto': forms.Select(attrs={'class':'form-control'}),

         }