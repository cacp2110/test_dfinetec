from django.db import models

from apps.cliente.models import Cliente
from apps.producto.models import Producto

class Venta(models.Model):
    cliente = models.ForeignKey(Cliente, null=True, blank=True, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, null=True, blank=True, on_delete=models.CASCADE)
    fecha = models.DateTimeField(null=True)
